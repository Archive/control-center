#include "capplet-info.h"
#include <gnome.h>
#include <libgnomeui/gnome-icon-entry.h>
#include <gconf/gconf-client.h>
#include <gconf/gconf-changeset.h>

typedef struct _CappletInfoPrivate CappletInfoPrivate;
struct _CappletInfoPrivate {
	CappletWidget *capplet;
	GConfClient *client;
	CappletInfo *info;
	GConfChangeSet *try_cs;
	GConfChangeSet *revert_cs;
	GladeXML *xml;
};

static CappletInfoPrivate cap_private;

#define CAPPLET_INFO_PRIVATE(info) ((CappletInfoPrivate *)info->private)

static void
capplet_info_destroy_callback (GtkWidget *widget,
			       CappletInfo *info)
{
	if (CAPPLET_INFO_PRIVATE (info)->client) {
		gtk_object_unref (GTK_OBJECT (CAPPLET_INFO_PRIVATE (info)->client));
	}
	if (CAPPLET_INFO_PRIVATE (info)->try_cs) {
		gconf_change_set_unref (CAPPLET_INFO_PRIVATE (info)->try_cs);
	}
	if (CAPPLET_INFO_PRIVATE (info)->revert_cs) {
		gconf_change_set_unref (CAPPLET_INFO_PRIVATE (info)->revert_cs);
	}
}

static void
capplet_info_state_changed (GtkWidget *widget, CappletInfo *info)
{
	GConfValue *val = NULL;

	if (info->changing != FALSE)
		return;

	capplet_widget_state_changed (CAPPLET_INFO_PRIVATE (info)->capplet, TRUE);
	if (info->get)
		val = (* info->get) (widget, info);
	if (val != NULL) {
		gconf_change_set_set_nocopy (CAPPLET_INFO_PRIVATE (info)->try_cs,
					     info->key, val);
	}
}

static void
capplet_info_try_callback (CappletWidget *capplet, CappletInfo *info)
{
	GtkWidget *widget;
	GConfValue *val;
	GConfChangeSet *old_try_cs;

	old_try_cs = CAPPLET_INFO_PRIVATE (info)->try_cs;
	CAPPLET_INFO_PRIVATE (info)->try_cs = gconf_change_set_new ();

	while (info->key) {
		widget = glade_xml_get_widget (CAPPLET_INFO_PRIVATE (info)->xml, info->glade_widget);
		if (widget == NULL) {
			g_warning (_("cannot find widget for %s\n"), info->glade_widget);
			info ++;
			continue;
		}

		if (gconf_change_set_check_value (old_try_cs,
						  info->key,
						  &val) != FALSE) {
			if (info->func) {
				if ((* info->func) (info, CAPPLET_SIGNAL_TRY, info->data) == FALSE) {
					gconf_client_set (CAPPLET_INFO_PRIVATE (info)->client, info->key, val, NULL);
				}
			} else {
				gconf_client_set (CAPPLET_INFO_PRIVATE (info)->client, info->key, val, NULL);
			}
		}
		gconf_change_set_unref (old_try_cs);
		gconf_change_set_ref (CAPPLET_INFO_PRIVATE (info)->try_cs);
		info++;
	}
}

static void
capplet_info_revert_callback (CappletWidget *capplet, CappletInfo *info)
{
	GtkWidget *widget;
	GConfValue *val;
 	gconf_client_commit_change_set (CAPPLET_INFO_PRIVATE (info)->client,
					CAPPLET_INFO_PRIVATE (info)->revert_cs,
					FALSE, NULL);
	while (info->key) {
		widget = glade_xml_get_widget (CAPPLET_INFO_PRIVATE (info)->xml, info->glade_widget);
		if (widget == NULL) {
			g_warning (_("cannot find widget for %s\n"), info->glade_widget);
			info ++;
			continue;
		}

		if (info->func) {
			if ((* info->func) (info, CAPPLET_SIGNAL_REVERT, info->data) != FALSE) {
				info++;
				continue;
			}
		}

		if (gconf_change_set_check_value (CAPPLET_INFO_PRIVATE (info)->revert_cs,
						  info->key,
						  &val) != FALSE) {
			/* The change should always be in the initial changeset */
			if (info->set) {
				info->changing = TRUE;
				(*info->set) (widget, info, val);
				info->changing = FALSE;
			}
		} else {
			g_assert_not_reached ();
		}
		info++;
	}
}

static void
capplet_info_ok_callback (CappletWidget *capplet, CappletInfo *info)
{
	GtkWidget *widget;
	GConfValue *val;

	while (info->key) {
		widget = glade_xml_get_widget (CAPPLET_INFO_PRIVATE (info)->xml, info->glade_widget);
		if (widget == NULL) {
			g_warning (_("cannot find widget for %s\n"), info->glade_widget);
			info ++;
			continue;
		}

		if (gconf_change_set_check_value (CAPPLET_INFO_PRIVATE (info)->try_cs,
						  info->key,
						  &val) != FALSE) {
			if (info->func) {
				if ((* info->func) (info, CAPPLET_SIGNAL_OK, info->data) == FALSE) {
					gconf_client_set (CAPPLET_INFO_PRIVATE (info)->client, info->key, val, NULL);
				}
			} else {
				gconf_client_set (CAPPLET_INFO_PRIVATE (info)->client, info->key, val, NULL);
			}
		}
		info++;
	}
}

static void
capplet_info_cancel_callback (CappletWidget *capplet, CappletInfo *info)
{
	GtkWidget *widget;
	GConfValue *val;

	while (info->key) {
		widget = glade_xml_get_widget (CAPPLET_INFO_PRIVATE (info)->xml, info->glade_widget);
		if (widget == NULL) {
			g_warning (_("cannot find widget for %s\n"), info->glade_widget);
			info ++;
			continue;
		}

		if (gconf_change_set_check_value (CAPPLET_INFO_PRIVATE (info)->revert_cs,
						  info->key,
						  &val) != FALSE) {
			if (info->func) {
				if ((* info->func) (info, CAPPLET_SIGNAL_CANCEL, info->data) == FALSE) {
					gconf_client_set (CAPPLET_INFO_PRIVATE (info)->client, info->key, val, NULL);
				}
			} else {
				gconf_client_set (CAPPLET_INFO_PRIVATE (info)->client, info->key, val, NULL);
			}
		}
		info++;
	}
}

static void
capplet_info_val_changed_callback (GConfClient *client,
				   const gchar *key,
				   GConfValue *value,
				   CappletInfo *info)
{
	GtkWidget *widget;
	g_print ("in capplet_info_val_changed_callback\n");

	while (info->key) {
		if (!strcmp (key, info->key)) {
			if (info->changing == FALSE) {
				widget = glade_xml_get_widget (CAPPLET_INFO_PRIVATE (info)->xml,
							       info->glade_widget);
				info->changing = TRUE;
				(* info->set) (widget, info, value);
				info->changing = FALSE;
			} 
		}
		info++;
	}
}

/* The specific setters and getters */
static void
capplet_info_radio_button_set (GtkWidget     *widget,
			       CappletInfo   *info,
			       GConfValue    *val)
{
	gchar *val_string;

	if (val != NULL) {
		switch (val->type) {
		case GCONF_VALUE_STRING:
			gtk_entry_set_text (GTK_ENTRY (widget), gconf_value_string (val));
			break;
		case GCONF_VALUE_INT:
		case GCONF_VALUE_FLOAT:
			val_string = gconf_value_to_string (val);
			gtk_entry_set_text (GTK_ENTRY (widget), val_string);
			g_free (val_string);
			break;
		default:
			g_warning ("Unknown key type in key %s\n", info->key);
			return;
		}
	} else {
		gtk_entry_set_text (GTK_ENTRY (widget), "");
	}
}

static GConfValue *
capplet_info_entry_get (GtkWidget     *widget,
			CappletInfo   *info)
{
	GConfValue *retval = NULL;
	gchar *text;
	gint i;
	gfloat f;

	text = gtk_entry_get_text (GTK_ENTRY (widget));
	retval = gconf_value_new (info->type);
	switch (info->type) {
	case GCONF_VALUE_STRING:
		gconf_value_set_string (retval, text);
		break;
	case GCONF_VALUE_INT:
		i = strtol (text, NULL, 0);
		gconf_value_set_int (retval, i);
		break;
	case GCONF_VALUE_FLOAT:
		f = strtod (text, NULL);
		gconf_value_set_float (retval, f);
		break;
	default:
		g_warning ("Unknown key type in key %s\n", info->key);
	}

	return retval;
}

static void
capplet_info_entry_set (GtkWidget     *widget,
			CappletInfo   *info,
			GConfValue    *val)
{
	gchar *val_string;

	if (val != NULL) {
		switch (val->type) {
		case GCONF_VALUE_STRING:
			gtk_entry_set_text (GTK_ENTRY (widget), gconf_value_string (val));
			break;
		case GCONF_VALUE_INT:
		case GCONF_VALUE_FLOAT:
			val_string = gconf_value_to_string (val);
			gtk_entry_set_text (GTK_ENTRY (widget), val_string);
			g_free (val_string);
			break;
		default:
			g_warning ("Unknown key type in key %s\n", info->key);
			return;
		}
	} else {
		gtk_entry_set_text (GTK_ENTRY (widget), "");
	}
}


void
capplet_info_set (CappletWidget *capplet,
		  GladeXML *xml,
		  CappletInfo *info)
{
	GtkWidget *widget;
	GConfClient *client = NULL;
	GConfValue *val;
	CappletInfoPrivate *private;
	GConfError *error = NULL;

	client = gconf_client_new ();
	gconf_client_add_dir (client, "/GNOME/Desktop/CappletTest", GCONF_CLIENT_PRELOAD_NONE, NULL);
	gtk_object_ref (GTK_OBJECT (client));
	gtk_object_sink (GTK_OBJECT (client));

	/* We can move to multiple private structs to support multi-capplets in
	 * the future */
	private = &cap_private;

	private->capplet = capplet;
	private->client = client;
	private->try_cs = gconf_change_set_new ();
	private->revert_cs = gconf_change_set_new ();
	private->xml = xml;

	gtk_signal_connect (GTK_OBJECT (capplet), "try", capplet_info_try_callback, info);
	gtk_signal_connect (GTK_OBJECT (capplet), "revert", capplet_info_revert_callback, info);
	gtk_signal_connect (GTK_OBJECT (capplet), "ok", capplet_info_ok_callback, info);
	gtk_signal_connect (GTK_OBJECT (capplet), "cancel", capplet_info_cancel_callback, info);
	gtk_signal_connect (GTK_OBJECT (client), "value_changed", capplet_info_val_changed_callback, info);

	while (info->key != NULL) {
		g_print ("looking at %s\n", info->glade_widget);
		widget = glade_xml_get_widget(xml, info->glade_widget);
		if (widget == NULL) {
			g_warning ("Cannot find widget %s in glade file", info->glade_widget);
			info ++;
			continue;
		}
		if (GTK_IS_CHECK_BUTTON (widget)) {
		} else if (GTK_IS_RADIO_BUTTON (widget)) {
		} else if (GTK_IS_SCALE (widget)) {
		} else if (GTK_IS_SPIN_BUTTON (widget)) { /* make sure we do it before entry */
		} else if (GTK_IS_ENTRY (widget)) {
			if (info->set == NULL)
				info->set = capplet_info_entry_set;
			if (info->get == NULL)
				info->get = capplet_info_entry_get;
			gtk_signal_connect (GTK_OBJECT (widget), "changed",
					    GTK_SIGNAL_FUNC (capplet_info_state_changed), info);
		} else if (GTK_IS_OPTION_MENU (widget)) {
		} else if (GNOME_IS_COLOR_PICKER (widget)) {
		} else if (GNOME_IS_ENTRY (widget)) {
		} else if (GNOME_IS_FONT_PICKER (widget)) {
		} else if (GNOME_IS_ICON_ENTRY (widget)) {
		} else {
			g_warning (_("Unknown type.\n\tWidget %s is not supported\n"), info->glade_widget);
		}

		/* initialize things */
		info->private = private;


		val = gconf_client_get_without_default (client, info->key, &error);
		if (error != NULL) {
			g_warning ("GConf error with key %s:\n  %s", info->key, error->str);
			gconf_error_destroy (error);
			info++;
			continue;
		}

		if (info->set) {
			info->changing = TRUE;
			(*info->set) (widget, info, val);
			info->changing = FALSE;
		}

		gtk_object_ref (GTK_OBJECT (CAPPLET_INFO_PRIVATE (info)->client));
		gconf_change_set_ref (CAPPLET_INFO_PRIVATE (info)->try_cs);
		gconf_change_set_ref (CAPPLET_INFO_PRIVATE (info)->revert_cs);

		gtk_signal_connect (GTK_OBJECT (widget), "destroy", capplet_info_destroy_callback, info);

		if (val != NULL) {
			/* FIXME: There is a bug here.  We will always set the
			 * value, independent of wether or not it's actually
			 * been set.  Thus, if we inadvertantly get the default
			 * value from one LOCALE, it will be set and present
			 * when we change even when we don't explicitly select
			 * it.  */
			gconf_change_set_set_nocopy (CAPPLET_INFO_PRIVATE (info)->revert_cs,
						     info->key, val);
		} else {
			gconf_change_set_unset (CAPPLET_INFO_PRIVATE (info)->revert_cs,
						info->key);
		}
		g_print ("just dealt with %s\n", info->glade_widget);
		info ++;
	}
	g_print ("info->key = %s\n", info->key);
	gtk_object_unref (GTK_OBJECT (private->client));
	gconf_change_set_unref (private->try_cs);
	gconf_change_set_unref (private->revert_cs);
}

