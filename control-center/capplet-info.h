#include "glib.h"
#include <gconf/gconf-value.h>
#include <gconf/gconf-client.h>
#include <glade/glade.h>
#include <capplet-widget.h>

typedef enum {
	CAPPLET_SIGNAL_TRY,
	CAPPLET_SIGNAL_REVERT,
	CAPPLET_SIGNAL_OK,
	CAPPLET_SIGNAL_CANCEL,
	CAPPLET_SIGNAL_DEFAULT
} CappletSignalType;

typedef struct _CappletInfo CappletInfo;

/* returns TRUE if it handled the revert fully; FALSE, if it wants the
 * engine to handle setting the widget correctly. */
typedef gint (*CappletInfoFunc)	(CappletInfo *info,
				 CappletSignalType type,
				 gpointer data);
/* Used to set the widget from the gconf value */
typedef void (*CappletInfoSet) (GtkWidget *widget,
				CappletInfo *info,
				GConfValue *val);

/* Used to get the gconf value from the widget */
/* the return value must be newly allocated */
typedef GConfValue *(*CappletInfoGet) (GtkWidget *widget,
				       CappletInfo *info);

struct _CappletInfo
{
	gchar *glade_widget;
	gchar *key;
	GConfValueType type;
	CappletInfoFunc func;
	gpointer data;
	CappletInfoSet set;
	CappletInfoGet get;
	gboolean changing;
	gpointer private;
};

void       capplet_info_set (CappletWidget *capplet,
			     GladeXML *xml,
			     CappletInfo *info);
       
