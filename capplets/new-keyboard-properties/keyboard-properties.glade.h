/*
 * Translatable strings file generated by Glade.
 * Add this file to your project's POTFILES.in.
 * DO NOT compile it as part of your application.
 */

gchar *s = N_("window1");
gchar *s = N_("Enable Keyboard Repeat");
gchar *s = N_("Delay Until Repeat");
gchar *s = N_("....a");
gchar *s = N_("...a");
gchar *s = N_("..a");
gchar *s = N_(".a");
gchar *s = N_("Key Repeat Rate");
gchar *s = N_("a....a");
gchar *s = N_("a...a");
gchar *s = N_("a..a");
gchar *s = N_("a.a");
gchar *s = N_("Enable Keyboard Click");
gchar *s = N_("Keyboard click");
gchar *s = N_("Click volume");
gchar *s = N_("Type here to test setting");
