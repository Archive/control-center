/*
 * Translatable strings file generated by extract-labels
 * Add this file to your project's POTFILES.in.
 * DO NOT compile it as part of your application.
 */

gchar *s = N_("Number of Colors.");
gchar *s = N_("Colors:");
gchar *s = N_("Number of circles to use.");
gchar *s = N_("Count:");
gchar *s = N_("Speed of Motion.");
gchar *s = N_("Faster");
gchar *s = N_("Slower");
gchar *s = N_("Display screensaver in monochrome.");
gchar *s = N_("Use a gradient of colors between circles.");
gchar *s = N_("Animate circles.");
gchar *s = N_("Cycle through colormap.");
