/*
 * Translatable strings file generated by Glade.
 * Add this file to your project's POTFILES.in.
 * DO NOT compile it as part of your application.
 */

gchar *s = N_("About \"<Screensaver name>\"");
gchar *s = N_("Draws quasiperiodic tilings; think of the implications on modern formica technology. Written by Time Korlove. In April 1997, Sir Roger Penrose, a British math professor who has worked with Stephen Hawking on such topics as relatvity, black holes, and whether time has a beginning, files a copyright-infringement lawsuit against Kimberly-Clark Corporation, wchih Penrose said copied a pattern he created ( apattern demonstrating that \"a nonrepeating pattern could exist in nature\") for its Kleenex quilted toilet paper. ");
gchar *s = N_("Configure Power Management");
gchar *s = N_("Shut down monitor after");
gchar *s = N_("minutes");
gchar *s = N_("Go to suspend mode after");
gchar *s = N_("minutes");
gchar *s = N_("Go to standby mode after");
gchar *s = N_("minutes");
gchar *s = N_("Settings for \"<Screensaver name>\"");
gchar *s = N_("There are no configurable settings for this\n"
              "screensaver. ");
gchar *s = N_("_Add");
gchar *s = N_("_Settings");
gchar *s = N_("_Remove");
gchar *s = N_("_About this screensaver...");
gchar *s = N_("window1");
gchar *s = N_("minutes");
gchar *s = N_("minutes");
gchar *s = N_("S_tart screensaver after ");
gchar *s = N_("S_witch screensavers after ");
gchar *s = N_("R_equire password to unlock screen");
gchar *s = N_("Enable _power management");
gchar *s = N_(" Configure...");
gchar *s = N_("Preview");
gchar *s = N_("_Mode:");
gchar *s = N_("Disable screensaver");
gchar *s = N_("Black screen only");
gchar *s = N_("One screensaver all the time");
gchar *s = N_("Random (checked screensavers)");
gchar *s = N_("Random (all screensavers)");
