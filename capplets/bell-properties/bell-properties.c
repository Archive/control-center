/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */
/* Author: Martin Baulig <martin@home-of-linux.org>
 * Based on gnome-core/desktop-properties/property-bell.c with
 * ideas from capplets/keyboard-properties/keyboard-properties.c.
 */

#ifdef HAVE_CONFIG_H
#   include <config.h>
#endif

#include "capplet-widget.h"
#include <stdio.h>
#include <stdarg.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include <X11/X.h>

#include <tree.h>
#include <parser.h>

#ifdef HAVE_X11_EXTENSIONS_XF86MISC_H
#include <X11/extensions/xf86misc.h>
#endif

#include "gnome.h"

static gint bell_percent;
static gint bell_pitch;
static gint bell_duration;
static XKeyboardState kbdstate;
static XKeyboardControl kbdcontrol;

static GtkObject *vadj, *padj, *dadj;
static GtkWidget *vscale, *pscale, *dscale;

static GtkWidget *capplet;

static void bell_read(void)
{
	bell_percent = gnome_config_get_int("/Desktop/Bell/percent=-1");
	bell_pitch = gnome_config_get_int("/Desktop/Bell/pitch=-1");
	bell_duration = gnome_config_get_int("/Desktop/Bell/duration=-1");

	XGetKeyboardControl(GDK_DISPLAY(), &kbdstate);

	if (bell_percent == -1) {
		bell_percent = kbdstate.bell_percent;
	}
	if (bell_pitch == -1) {
	        bell_pitch = kbdstate.bell_pitch;
        }
	if (bell_duration == -1) {
	        bell_duration = kbdstate.bell_duration;
	}
}

static void read_from_xml (xmlDocPtr doc) 
{
        xmlNodePtr root_node, node;

        root_node = xmlDocGetRootElement (doc);
        if (strcmp (root_node->name, "bell-prefs")) return;

        for (node = root_node->childs; node; node = node->next) {
                if (!strcmp (node->name, "percent"))
                        bell_percent = atoi (xmlNodeGetContent (node));
                else if (!strcmp (node->name, "pitch"))
                        bell_pitch = atoi (xmlNodeGetContent (node));
                else if (!strcmp (node->name, "duration"))
                        bell_duration = atoi (xmlNodeGetContent (node));
        }
}

static xmlDocPtr write_to_xml (void) 
{
        xmlDocPtr doc;
        xmlNodePtr root_node;
        char *str;

        doc = xmlNewDoc ("1.0");
        root_node = xmlNewDocNode (doc, NULL, "bell-prefs", NULL);
        xmlDocSetRootElement (doc, root_node);

        str = g_strdup_printf ("%d", bell_percent);
        xmlNewChild (root_node, NULL, "percent", str);
        g_free (str);

        str = g_strdup_printf ("%d", bell_pitch);
        xmlNewChild (root_node, NULL, "pitch", str);
        g_free (str);

        str = g_strdup_printf ("%d", bell_duration);
        xmlNewChild (root_node, NULL, "duration", str);
        g_free (str);

        return doc;
}

static void bell_help(void)
{
  gchar *tmp;

  tmp = gnome_help_file_find_file ("users-guide", "gccmm.html#GCC-KEYBELL");
  if (tmp) {
    gnome_help_goto(0, tmp);
    g_free(tmp);
  } else {
          GtkWidget *mbox;

          mbox = gnome_message_box_new(_("No help is available/installed for these settings. Please make sure you\nhave the GNOME User's Guide installed on your system."),
                                       GNOME_MESSAGE_BOX_ERROR,
                                       _("Close"), NULL);

          gtk_widget_show(mbox);
  }
}

static void bell_apply(void)
{
	kbdcontrol.bell_percent = bell_percent;
	kbdcontrol.bell_pitch = bell_pitch;
	kbdcontrol.bell_duration = bell_duration;
	XChangeKeyboardControl(GDK_DISPLAY(), KBBellPercent | KBBellPitch | KBBellDuration, &kbdcontrol);
}

static void bell_write(void)
{
	bell_apply();
	gnome_config_set_int("/Desktop/Bell/percent", bell_percent);
	gnome_config_set_int("/Desktop/Bell/pitch", bell_pitch);
	gnome_config_set_int("/Desktop/Bell/duration", bell_duration);
	gnome_config_sync ();
}
static void
bell_revert (void)
{
        bell_read();
        bell_apply();
        GTK_ADJUSTMENT (vadj)->value = bell_percent;
        GTK_ADJUSTMENT (padj)->value = bell_pitch;
        GTK_ADJUSTMENT (dadj)->value = bell_duration;
        gtk_adjustment_changed (GTK_ADJUSTMENT (vadj));
        gtk_adjustment_changed (GTK_ADJUSTMENT (padj));
        gtk_adjustment_changed (GTK_ADJUSTMENT (dadj));
}

/* Run when a scale widget is manipulated.  */
static void
scale_moved (GtkAdjustment *adj, gpointer data)
{
        int *value = (int *) data;
        *value = adj->value;
        capplet_widget_state_changed(CAPPLET_WIDGET (capplet), TRUE);
}

static void
bell_test(GtkWidget *widget, void *data)
{
        gint save_percent;
	gint save_pitch;
	gint save_duration;

	XGetKeyboardControl(GDK_DISPLAY(), &kbdstate);

	save_percent = kbdstate.bell_percent;
	save_pitch = kbdstate.bell_pitch;
	save_duration = kbdstate.bell_duration;
	
	kbdcontrol.bell_percent = bell_percent;
	kbdcontrol.bell_pitch = bell_pitch;
	kbdcontrol.bell_duration = bell_duration;
	XChangeKeyboardControl(GDK_DISPLAY(), KBBellPercent | KBBellPitch | KBBellDuration, &kbdcontrol);

        XBell (gdk_display,0);

	kbdcontrol.bell_percent = save_percent;
	kbdcontrol.bell_pitch = save_pitch;
	kbdcontrol.bell_duration = save_duration;
	XChangeKeyboardControl(GDK_DISPLAY(), KBBellPercent | KBBellPitch | KBBellDuration, &kbdcontrol);
}

static GtkWidget *
make_scale (char *title, GtkObject *adjust, int *update_var, GtkWidget *table, int row)
{
        GtkWidget *scale, *ttl;

        ttl = gtk_label_new (title);

        gtk_misc_set_alignment (GTK_MISC (ttl), 0.0, 0.5);
        gtk_table_attach (GTK_TABLE (table), ttl,
                          0, 1, row, row + 1,
                          GTK_FILL | GTK_SHRINK,
                          GTK_FILL | GTK_SHRINK,
                          0, 0);
        gtk_widget_show (ttl);

        scale = gtk_hscale_new (GTK_ADJUSTMENT (adjust));
        gtk_range_set_update_policy (GTK_RANGE (scale), GTK_UPDATE_CONTINUOUS);
        gtk_scale_set_digits (GTK_SCALE (scale), 0);
        gtk_signal_connect (GTK_OBJECT (adjust), "value_changed",
                            GTK_SIGNAL_FUNC (scale_moved),
                            (gpointer) update_var);

        gtk_table_attach (GTK_TABLE (table), scale,
                          1, 2, row, row + 1,
                          GTK_EXPAND | GTK_FILL | GTK_SHRINK,
                          GTK_FILL | GTK_SHRINK,
                          0, 0);
        gtk_widget_show (scale);

        return scale;
}

static void
bell_setup(void)
{
	GtkWidget *vbox;
	GtkWidget *frame;
	GtkWidget *table;
        GtkWidget *test, *hbox, *inner_hbox, *volume;

        capplet = capplet_widget_new();

	vbox = gtk_vbox_new(FALSE, GNOME_PAD_SMALL);
	gtk_container_set_border_width(GTK_CONTAINER(vbox), GNOME_PAD);

	frame = gtk_frame_new(_("Keyboard Bell"));
	gtk_box_pack_start (GTK_BOX (vbox), frame, FALSE, FALSE, 0);

	table = gtk_table_new (4, 2, FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (table), GNOME_PAD);
	gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
	gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
	gtk_container_add (GTK_CONTAINER (frame), table);

	
	vadj = gtk_adjustment_new(bell_percent, 0, 100, 1, 1, 0);
	vscale = make_scale(_("Volume"), vadj, &bell_percent, table, 1);

	padj = gtk_adjustment_new(bell_pitch, 0, 2000, 1, 1, 0);
        pscale = make_scale (_("Pitch (Hz)"), padj, &bell_pitch, table, 2);

	dadj = gtk_adjustment_new(bell_duration, 0, 500, 1, 1, 0);
	dscale = make_scale(_("Duration (ms)"), dadj, &bell_duration, table, 3);

	/* Finished */

        gtk_signal_connect (GTK_OBJECT (capplet), "help",
                            GTK_SIGNAL_FUNC (bell_help), NULL);
        gtk_signal_connect (GTK_OBJECT (capplet), "try",
                            GTK_SIGNAL_FUNC (bell_apply), NULL);
        gtk_signal_connect (GTK_OBJECT (capplet), "revert",
                            GTK_SIGNAL_FUNC (bell_revert), NULL);
        gtk_signal_connect (GTK_OBJECT (capplet), "ok",
                            GTK_SIGNAL_FUNC (bell_write), NULL);
        gtk_signal_connect (GTK_OBJECT (capplet), "cancel",
                            GTK_SIGNAL_FUNC (bell_revert), NULL);

        test = gtk_button_new ();
        hbox = gtk_hbox_new (FALSE, 0);
        gtk_box_pack_start (GTK_BOX (hbox), test, FALSE, FALSE, 0);
        gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
        gtk_signal_connect (GTK_OBJECT (test), "clicked",
                            GTK_SIGNAL_FUNC (bell_test), NULL);
        inner_hbox = gtk_hbox_new (FALSE, 0);
        volume = gnome_stock_pixmap_widget (test, GNOME_STOCK_PIXMAP_VOLUME);
        gtk_box_pack_start (GTK_BOX (inner_hbox), volume, FALSE, FALSE, 0);
        gtk_box_pack_start (GTK_BOX (inner_hbox), gtk_label_new (_("Test")), FALSE, FALSE, 8);
        gtk_container_add (GTK_CONTAINER (test), inner_hbox);

        gtk_container_add (GTK_CONTAINER (capplet), vbox);
        gtk_widget_show_all (capplet);
}


static void do_get_xml (void) 
{
        xmlDocPtr doc;

        bell_read ();
        doc = write_to_xml ();
        xmlDocDump (stdout, doc);
}

static void do_set_xml (void) 
{
        xmlDocPtr doc;
	char *buffer;
	int len = 0;

	while (!feof (stdin)) {
		if (!len) buffer = g_new (char, 16384);
		else buffer = g_renew (char, buffer, len + 16384);
		fread (buffer + len, 1, 16384, stdin);
		len += 16384;
	}

	doc = xmlParseMemory (buffer, strlen (buffer));

	read_from_xml (doc);
        bell_apply ();
}

int
main (int argc, char **argv)
{
        GnomeClient *client = NULL;
        GnomeClientFlags flags;
        gchar *session_args[3];
        int token, init_results;

        bindtextdomain (PACKAGE, GNOMELOCALEDIR);
        textdomain (PACKAGE);

	init_results = gnome_capplet_init("bell-properties", VERSION,
					  argc, argv, NULL, 0, NULL);
	if (init_results < 0) {
                g_warning (_("an initialization error occurred while "
			   "starting 'bell-properties-capplet'.\n"
                           "aborting...\n"));
                exit (1);
	}
        else if (init_results == 3) {
                do_get_xml ();
                return 0;
        }
        else if (init_results == 4) {
                do_set_xml ();
                return 0;
        }

	client = gnome_master_client ();
	flags = gnome_client_get_flags(client);

	if (flags & GNOME_CLIENT_IS_CONNECTED) {
		token = gnome_startup_acquire_token("GNOME_BELL_PROPERTIES",
				                  gnome_client_get_id(client));

		if (token) {
			session_args[0] = argv[0];
			session_args[1] = "--init-session-settings";
			session_args[2] = NULL;
			gnome_client_set_priority (client, 20);
			gnome_client_set_restart_style (client, 
							GNOME_RESTART_ANYWAY);
			gnome_client_set_restart_command (client, 2, 
							  session_args);
		}
		else 
			gnome_client_set_restart_style (client, 
							GNOME_RESTART_NEVER);

                gnome_client_flush (client);
        }
	else
		token = 1;

        bell_read ();	

        if(token) 
                bell_apply ();

	if (init_results != 1) {
		bell_setup ();
	        capplet_gtk_main ();
	}
	return 0;
}
